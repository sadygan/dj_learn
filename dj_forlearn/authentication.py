from rest_framework_jwt.authentication import JSONWebTokenAuthentication, BaseJSONWebTokenAuthentication
from rest_framework_jwt.settings import api_settings
from rest_framework.authentication import BaseAuthentication
from rest_framework_jwt.authentication import jwt_get_username_from_payload
from rest_framework.authentication import BaseAuthentication
import jwt
from attrdict import AttrDict
from rest_framework_jwt.authentication import BaseJSONWebTokenAuthentication

import hashlib

jwt_decode_handler = api_settings.JWT_DECODE_HANDLER
jwt_get_username_from_payload = api_settings.JWT_PAYLOAD_GET_USERNAME_HANDLER
from rest_framework import exceptions


class JSONWebTokenAuthentication(JSONWebTokenAuthentication):
    def authenticate(self, request):
        """
        Returns a two-tuple of `User` and token if a valid signature has been
        supplied using JWT-based authentication.  Otherwise returns `None`.
        """
        jwt_value = self.get_jwt_value(request)

        if jwt_value is None:
            return None

        try:
            payload = jwt_decode_handler(jwt_value)
        except jwt.ExpiredSignature:
            msg = ('Signature has expired.')
            raise exceptions.AuthenticationFailed(msg)
        except jwt.DecodeError:
            msg = ('Error decoding signature.')
            raise exceptions.AuthenticationFailed(msg)
        except jwt.InvalidTokenError:
            raise exceptions.AuthenticationFailed()

        user = self.authenticate_credentials(payload)

        return user, jwt_value

    def authenticate_credentials(self, payload):
        """
        Returns an active user that matches the payload's user id and email.
        """
        username = jwt_get_username_from_payload(payload)
        print("----------")
        if not username:
            msg = _('Invalid payload.')
            raise exceptions.AuthenticationFailed(msg)

        if username in ('vasya', 'petya'):
            # TODO: oracle authorization?
            user = User(username)

        else:
            msg = _('Invalid signature.')
            raise exceptions.AuthenticationFailed(msg)

        # if not user.is_active:
        #     msg = _('User account is disabled.')
        #     raise exceptions.AuthenticationFailed(msg)

        return user


class User:
    is_active = True
    is_authenticated = True

    def __init__(self, username):
        self.username = username
        # self._meta = AttrDict(pk={'value_to_string': lambda user: None})

    @property
    def pk(self):
        return self.username
    #     return int(hashlib.sha1(self.username.encode()).hexdigest(), 16)

    # def save(self, **kwargs):
    #     pass


class CustomBackend:
    def authenticate(self, request, username=None, password=None):
        # TODO: oracle authentication
        return User(username)

    # Not used
    # def get_user(self, user_id):
    #     print('USER_ID=', user_id)
    #     return None
