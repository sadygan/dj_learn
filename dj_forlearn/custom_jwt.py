from rest_framework_jwt.compat import get_username
from rest_framework_jwt.settings import api_settings
from datetime import datetime
import warnings


def jwt_payload_handler(user, request):
    print(request.META.get('HTTP_X_FORWARDED_FOR'))
    import pdb; pdb.set_trace()
    username = get_username(user)

    warnings.warn(
        'The following fields will be removed in the future: '
        '`email` and `user_id`. ',
        DeprecationWarning
    )

    payload = {
        'username': username,
        'exp': datetime.utcnow() + api_settings.JWT_EXPIRATION_DELTA,

    }
    if hasattr(user, 'email'):
        payload['email'] = user.email

    if api_settings.JWT_AUDIENCE is not None:
        payload['aud'] = api_settings.JWT_AUDIENCE

    if api_settings.JWT_ISSUER is not None:
        payload['iss'] = api_settings.JWT_ISSUER

    return payload
