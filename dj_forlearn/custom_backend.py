from rest_framework.response import Response
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from rest_framework_jwt.authentication import jwt_get_username_from_payload
from attrdict import AttrDict
from django.contrib.auth import authenticate, login

import hashlib


class JSONWebTokenAuthentication(JSONWebTokenAuthentication):
    def authenticate_credentials(self, payload):
        """
        Returns an active user that matches the payload's user id and email.
        """
        if not username:
            msg = _('Invalid payload.')
            raise exceptions.AuthenticationFailed(msg)

        if username in ('vasya', 'petya'):
            # TODO: oracle authorization?
            user = User(username)

        else:
            msg = _('Invalid signature.')
            raise exceptions.AuthenticationFailed(msg)

        if not user.is_active:
            msg = _('User account is disabled.')
            raise exceptions.AuthenticationFailed(msg)

        return user


class User:
    is_active = True
    is_authenticated = True

    def __init__(self, username):
        self.username = username
        # self._meta = AttrDict(pk={'value_to_string': lambda user: None})

    @property
    def pk(self):
        return self.username
    #     return int(hashlib.sha1(self.username.encode()).hexdigest(), 16)

    def save(self, **kwargs):
        pass


class CustomBackend:

    def authenticate(self, request, username=None, password=None):
        # TODO: oracle authentication
        user = User(username)
        return user

    # Not used
    # def get_user(self, user_id):
    #     print('USER_ID=', user_id)
    #     return None
