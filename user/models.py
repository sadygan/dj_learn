from django.db import models


class User2(models.Model):
    name = models.CharField(max_length=200)
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    phone = models.CharField(max_length=18)
    address = models.CharField(max_length=50)

    class Meta:
        managed = False
