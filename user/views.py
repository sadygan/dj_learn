from rest_framework.views import APIView
from rest_framework import permissions, response, status, serializers
from .serializers import User2Serializers


DATA = {
    'name': 'name 1',
    'first_name': 'first name 1',
    'last_name': 'last name 1',
    'phone': '002020002',
    'address': 'qwewqe wqeqwe'
}

class User2Flow(APIView):

    # permission_classes = permissions.AllowAny

    def get(self, request, *args, **kwargs):
        user = User2Serializers(DATA)
        return response.Response(user.data, status=status.HTTP_200_OK)