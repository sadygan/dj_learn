from rest_framework import serializers
from .models import User2


class User2Serializers(serializers.ModelSerializer):
    class Meta:
        model = User2
        fields = ('name', 'first_name', 'last_name', 'phone',)
