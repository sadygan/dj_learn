from rest_framework.views import APIView
from drf_yasg.utils import swagger_auto_schema

from drf_yasg import openapi
from . import serializers
from rest_framework.response import Response


class SimpeView(APIView):
    # swagger_schema = None

    @swagger_auto_schema(operation_id='hallo')
    def get(self, request):
        return Response({'how': 'are you'})

    @swagger_auto_schema()
    def post(self):
        return ''
