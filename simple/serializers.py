from rest_framework import serializers


class SimpleSerializer(serializers.Serializer):
    field1 = serializers.CharField()
    field2 = serializers.CharField()
    field3 = serializers.CharField()
    field4 = serializers.CharField()
